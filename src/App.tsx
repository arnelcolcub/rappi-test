import { Navigation } from "react-native-navigation";
import useViews from "./views";

const { useScreens } = useViews();
const { Home, Settings } = useScreens();

Navigation.registerComponent("Home", () => Home);
Navigation.registerComponent("Settings", () => Settings);

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
            stack: {
                children: [
                    {
                        component: {
                            name: "Home"
                        }
                    }
                ]
            }
        }
    })
});