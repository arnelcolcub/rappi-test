import Home from "./Home";
import Settings from "./Settings";

const useScreens = () => {
    return {
        Home,
        Settings
    };
}

export default useScreens;