import React, { FC } from "react";
import { Button, Text } from "react-native";
import { Navigation } from "react-native-navigation";
import { ComponentProps } from "../../../models/interfaces/general";

const Home : FC<ComponentProps> = (props) => {
    /** Props */
    const { componentId } = props;
    return (
        <React.Fragment>
            <Text>Hola Soy el Home!</Text>
            <Button
                onPress={() => Navigation.push(componentId, {
                    component: {
                        name: "Settings",
                        options: {
                            topBar: {
                                title: {
                                    text: "Configuración"
                                }
                            }
                        }
                    }
                })}
                title="Vamos a Configuracion!!!."
            />
        </React.Fragment>
    );
}

export default Home;